var sql = require('../connects/con_db_xiemenhePJ');
var request = require("request");
var service = {};
var btoa = require('btoa');
var atob = require('atob');

service.encode = async function (datajson) {
  return btoa(datajson);
};
service.decode = async function (datajson) {
  return atob(datajson);
};
service.gencode = function (length) {
  var result = "";
  var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(
      Math.floor(Math.random() * charactersLength)
    );
  }
  return result;
};
service.plustime = function (length) {
  var nows = new Date();
  // nows.setMinutes(nows.getMinutes() + length); // + x นาที 
  nows.setHours(nows.getHours() + length); // + x นาที 
  now = new Date(nows); // Date object
  var day = String(now.getDate()).padStart(2, "0");
  var month = String(now.getMonth() + 1).padStart(2, "0");
  var expirydate = now.getFullYear() + "/" + month + "/" + day + " " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
  // console.log("fn plustime" + expirydate );
  return expirydate;
};
service.plusyear = function (length) {
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, "0");
  var mm = String(today.getMonth() + 1).padStart(2, "0");
  var yyyy = today.getFullYear() + length;
  var times = today.toTimeString();
  var expirydate = yyyy + "/" + mm + "/" + dd + " " + times.split(" ")[0];
  return expirydate;
};
service.content = function (data_con, callback) {
  var { prj, seapp, pbip, syss, chi } = data_con;
  sql.query("SELECT * FROM xiemenhe_session WHERE  session = '" + syss + "' AND NOW() < expire_date", (err, resulta) => {
    // data callback
    var data = {
      status: "",
      premium:"",
      name:"",
      des:"",
      logo:""
    };
    if (err) {
      return res.status(500).send(err);
    } else {
      if (resulta.length > 0) {
        data.status = "1"
        sql.query("SELECT * FROM xiemenhe_contentdata WHERE id = '"+chi+"'",  (err, resultb) => {
              data.name = resultb[0].charnal_name
              data.des = resultb[0].description
              data.logo = resultb[0].logo1
            if(resultb[0].premium == "1"){
              //content premium
              sql.query("SELECT * FROM xiemenhe_usergen WHERE user_gen = '"+resulta[0].user_gen+"' AND NOW() < expire_date", (err, result) => {
                if(result.length > 0){
                  //User  non exprire
                  var dataexpire = result[0].expire_date;
                  var expire = service.subtract(dataexpire);
                  console.log(expire)
                  data.premium = expire
                  callback(null, data);
                }else{
                  //User exprire
                  data.premium = "หมดอายุ"
                  callback(null, data);
                }
              });
            }else{
              //content Not premium
              console.log("dddddd")
              data.premium = "ช่องฟรี"
              callback(null, data);
            }
        });
      } else {
        data.status = "0"
        callback(null, data);
      }
      
    }
  });

};
service.link_troken = function (data_content, callback) {
  var { prj, seapp, pbip, syss, chi } = data_content;
  sql.query("SELECT * FROM xiemenhe_contentdata WHERE id = '" + chi + "'", (err, result) => {
    if (err) {
      return res.status(500).send(err);
    } else {
      var options = {
        method: 'GET',
        url: 'http://96.30.124.237:3000/maintenance'
      };

      request(options, function (error, response, body) {
        if (error) throw new Error(error);
        console.log(body);
        callback(null,body);
      });
    }
  });

};
service.subtract =  function (exp_date){
  var expdate = new Date(exp_date);
  now = new Date(); // Date object
  var day = String(now.getDate()).padStart(2, "0");
  var month = String(now.getMonth() + 1).padStart(2, "0");
  var times = now.toTimeString();
  var nowdate = now.getFullYear() + "/" + month + "/" + day + " " + times.split(" ")[0];
  var nowdates = new Date(nowdate)
  var milisectime = (exp_date - nowdates) ;
  console.log(exp_date);
  console.log(nowdates);
  console.log(milisectime);
  return milisectime.toString();
}

module.exports = service;