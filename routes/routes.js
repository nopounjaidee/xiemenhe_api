
var btoa = require('btoa');
var atob = require('atob');
module.exports = function(app)
{   
    const {encodemethod,decodemethod,authen} = require('../package/controllers/controller_authen');
    const {contents,lam} = require('../package/controllers/controller_content');
    app.post('/enc/:encjson', encodemethod);
    app.post('/dec/:decjson', decodemethod);
    app.post('/authen', authen);
    app.post('/contents', contents);
    app.post('/lam',lam);

    app.use(function(req, res, next) {
        console.log("Link Part ไม่ถูกต้อง !! ");
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });
    app.use(function(err, req, res, next) {
        log.error("มีบางอย่างผิดพลาด !!! :", err);
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    });
};