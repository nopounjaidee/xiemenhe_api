var express = require('express');
var bodyparser = require('body-parser');
var cookieParser = require('cookie-parser');

module.exports = function()
{
    var app = express();
    app.use(bodyparser.json());
    app.use(bodyparser.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.set('port', process.env.PORT || 3000);
    app.use(function(req,res,next){
        res.setHeader('Access-Control-Allow-Origin','*');
        //res.setHeader('Access-Control-Allow-Origin','http://localhost:4200');
        res.setHeader('Access-Control-Allow-Methods','GET, POST, PUT, DELETE');
        res.setHeader('Access-Control-Allow-Headers','content-type, x-access-token');
        res.setHeader('Access-Control-Allow-Credentials','*');
        next();
    });

    require('../routes/routes')(app);
    return app;
};